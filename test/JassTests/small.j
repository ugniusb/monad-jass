//////////////////// declarations

type nh extends handle

globals
    constant integer max_skel = 123
    real skelspawn = 0
endglobals

constant native GetWidgetLife takes handle unit returns real


//////////////////// functions

constant function lolk takes integer val returns nothing
    local integer i = 0

    loop
        exitwhen i > 10
        set i = i + 1
        set skelspawn[i] = 0
    endloop
    if (i == 10) then
        set i = 11
    elseif (i == 11) then
        set i = 12
    else
        set i = 999
    endif
endfunction

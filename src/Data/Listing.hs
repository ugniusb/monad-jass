{-# LANGUAGE DeriveFunctor #-}
module Data.Listing (
      ListingF (..)
    , Listing
    , Listing'
    , lift
    , foldListing
    , unfoldListing
) where

import           Control.Monad.Free (Free, liftF)
import           Control.Monad.Trans.Free (FreeF (..))
import           Data.Functor.Foldable


data ListingF a x =
    ListingF a x
    deriving (Functor)


type Listing a = Free (ListingF a)
type Listing' a = Listing a ()


lift :: a -> Listing a ()
lift x = liftF (ListingF x ())


foldListing :: Listing a x -> [a]
foldListing = cata alg
    where
        alg :: FreeF (ListingF a) x [a] -> [a]
        alg (Pure _) = []
        alg (Free (ListingF x xs)) = x : xs


unfoldListing :: [a] -> Listing a ()
unfoldListing = ana alg
    where
        alg :: [a] -> FreeF (ListingF a) () [a]
        alg [] = Pure ()
        alg (x:xs) = Free (ListingF x xs)

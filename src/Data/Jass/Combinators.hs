{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.Jass.Combinators (
    -- * Root of a JASS AST
      program
    -- * JASS declarations
    -- ** Subtyping
    , extends
    -- ** Globals
    , globals
    , gconst        -- | Global constant
    , gvar          -- | Global variable with initial value
    , gvar'         -- | Global variable
    , garray        -- | Global array
    -- ** Native functions
    , native        -- | Impure native function
    , cnative       -- | Constant native function
    -- * JASS definitions
    -- ** Functions
    , xfunction     -- | Generic function
    , function      -- | Impure function
    , cfunction     -- | Constant function
    , arg           -- | Function argument
    -- ** Locals
    , lvar          -- | Local variable with initial value
    , lvar'         -- | Local varianble
    , larray        -- | Local array
    -- ** Variables
    , ref           -- | Reference a named variable in an expression
    , at            -- | Indexing an array
    , set           -- | Modifying a variable
    -- ** Flow control
    , run           -- | Call a function ignoring the result
    -- *** Conditionals
    , case'         -- | Conditional case
    , if'           -- | Root of an if-elif-else tree
    , elif          -- | An (else)if case
    , else'         -- | An else case
    -- *** Looping
    , loop          -- | Loop
    , inl           -- | Lift statements into a loop
    , exitwhen      -- | Break out of a loop when condition is true
    -- ** Expressions
    , call          -- | Call a function
    , (+.)
    , (-.)
    , (*.)
    , (/.)
    , (==.)
    , (/=.)
    , (>.)
    , (<.)
    , (>=.)
    , (<=.)
    , (||.)
    , (&&.)
    , negate'
    , not'
    -- ** Literals
    , int
    , octal
    , hex
    , fourcc        -- | 4 char encoding, eg 'hpea' for Human Peasant
    , real
    , bool
) where

import           Data.Jass.Internal
import           Data.List.NonEmpty (NonEmpty)
import           Data.Listing
import           Data.Semigroup
import           Data.String


type Some f = Listing' (f ())
type Some' f = Listing' f


newtype ArrRef =
    ArrRef Name
    deriving (IsString)


data VarRef = VarRef
    { getVarName :: Name
    , getVarIndex :: Maybe (Expr ())
    }

instance IsString VarRef where
    fromString n = VarRef (Name n) Nothing


newtype OpenIfCases = Open (NonEmpty (Case ()))

newtype ClosedIfCases = Closed (CaseStatement ())

class IfCases a where
    getCases :: a -> NonEmpty (Case ())
    getDefaultCase :: a -> Maybe [Statement ()]

instance IfCases OpenIfCases where
    getCases (Open xs) = xs
    getDefaultCase _ = Nothing

instance IfCases ClosedIfCases where
    getCases (Closed x) = caseConditionals x
    getDefaultCase (Closed x) = caseDefault x


program :: Some Declr -> Some Function -> Program ()
program declrs funcs =
    Program (foldListing declrs) (foldListing funcs)


extends :: Name -> Name -> Some Declr
extends new old =
    lift $ Typedef new old


globals :: Some Global -> Some Declr
globals xs =
    lift $ Globals (foldListing xs)


gconst :: Type -> Name -> Expr () -> Some Global
gconst t n v =
    lift $ GlobalConst (VarDeclr t n) v


gvar :: Type -> Name -> Expr () -> Some Global
gvar t n v =
    lift $ GlobalVar (SimpleVar (VarDeclr t n) (Just v))


gvar' :: Type -> Name -> Some Global
gvar' t n =
    lift $ GlobalVar (SimpleVar (VarDeclr t n) Nothing)


garray :: Type -> Name -> Some Global
garray t n =
    lift $ GlobalVar (ArrayVar (VarDeclr t n))


xnative :: Constness -> Maybe Type -> Name -> Some' VarDeclr -> Some Declr
xnative c t n a =
    lift . Native c $ FuncDeclr n (foldListing a) t


native :: Maybe Type -> Name -> Some' VarDeclr -> Some Declr
native = xnative Variable


cnative :: Maybe Type -> Name -> Some' VarDeclr -> Some Declr
cnative = xnative Constant


xfunction :: Constness -> Maybe Type -> Name -> Some' VarDeclr
         -> Some Local
         -> Some Statement
         -> Some Function
xfunction c t n a l s =
    lift $ Function
        c
        (FuncDeclr n (foldListing a) t)
        (foldListing l)
        (foldListing s)

function :: Maybe Type -> Name -> Some' VarDeclr
         -> Some Local
         -> Some Statement
         -> Some Function
function = xfunction Variable


cfunction :: Maybe Type -> Name -> Some' VarDeclr
          -> Some Local
          -> Some Statement
          -> Some Function
cfunction = xfunction Constant


arg :: Type -> Name -> Some' VarDeclr
arg t n =
    lift $ VarDeclr t n


lvar :: Type -> Name -> Expr () -> Some Local
lvar t n v =
    lift . Local $ SimpleVar (VarDeclr t n) (Just v)


lvar' :: Type -> Name -> Some Local
lvar' t n =
    lift . Local $ SimpleVar (VarDeclr t n) Nothing


larray :: Type -> Name -> Some Local
larray t n =
    lift . Local . ArrayVar $ VarDeclr t n


ref :: VarRef -> Expr ()
ref VarRef{..} =
    case getVarIndex of
        Nothing -> NameRef () getVarName
        Just i -> ArrayRef () getVarName i


run :: Name -> Some Expr -> Some Statement
run n a =
    lift . CallSttmnt $ CallStatement n (foldListing a)


if' :: IfCases i => i -> Some Statement
if' x =
    lift $ IfElifSttmnt $ CaseStatement (getCases x) (getDefaultCase x)


case' :: Expr () -> Some Statement -> OpenIfCases
case' c s =
    Open $ pure $ Case c (foldListing s)


elif :: OpenIfCases -> OpenIfCases -> OpenIfCases
elif (Open l) (Open r) =
    Open $ l <> r


else' :: OpenIfCases -> Some Statement -> ClosedIfCases
else' (Open c) s =
    Closed $ CaseStatement c $ Just (foldListing s)


loop :: Some InLoopStatement -> Some Statement
loop =
    lift . LoopSttmnt . LoopStatement . foldListing


inl :: Some Statement -> Some InLoopStatement
inl =
    unfoldListing . map UsualStatement . foldListing


exitwhen :: Expr () -> Some InLoopStatement
exitwhen =
    lift . ExitWhen


set :: VarRef -> Expr () -> Some Statement
set VarRef{..} v =
    lift . SetSttmnt $ SetStatement getVarName getVarIndex v


at :: ArrRef -> Expr () -> VarRef
at (ArrRef n) i =
    VarRef n $ Just i


call :: Name -> Some Expr -> Some Expr
call n a =
    lift $ FuncCall () n (foldListing a)


(+.) :: Expr () -> Expr () -> Expr ()
(+.) = BinExpr () JAdd


(-.) :: Expr () -> Expr () -> Expr ()
(-.) = BinExpr () JSub


(*.) :: Expr () -> Expr () -> Expr ()
(*.) = BinExpr () JMul


(/.) :: Expr () -> Expr () -> Expr ()
(/.) = BinExpr () JDiv


(==.) :: Expr () -> Expr () -> Expr ()
(==.) = BinExpr () JEqual


(/=.) :: Expr () -> Expr () -> Expr ()
(/=.) = BinExpr () JNotEqual


(>.) :: Expr () -> Expr () -> Expr ()
(>.) = BinExpr () JGreater


(<.) :: Expr () -> Expr () -> Expr ()
(<.) = BinExpr () JLess


(>=.) :: Expr () -> Expr () -> Expr ()
(>=.) = BinExpr () JGreaterEq


(<=.) :: Expr () -> Expr () -> Expr ()
(<=.) = BinExpr () JLessEq


(||.) :: Expr () -> Expr () -> Expr ()
(||.) = BinExpr () JOr

(&&.) :: Expr () -> Expr () -> Expr ()
(&&.) = BinExpr () JAnd


negate' :: Expr () -> Expr ()
negate' = UnExpr () JUNeg


not' :: Expr () -> Expr ()
not' = UnExpr () JUNot


int :: Int -> Expr ()
int =
    IntLit . DecimalLit


octal :: Int -> Expr ()
octal =
    IntLit . OctalLit


hex :: Int -> Expr ()
hex =
    IntLit . HexLit


fourcc :: (Char, Char, Char, Char) -> Expr ()
fourcc (x, y, z, w) =
    IntLit $ FourCC x y z w


real :: Float -> Expr ()
real = RealLit


bool :: Bool -> Expr ()
bool = BoolLit

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveFunctor #-}
module Data.Jass.Internal (
      Program (..)
    , Declr (..)
    , Constness (..)
    , Global (..)
    , FuncDeclr (..)
    , Function (..)
    , Local (..)
    , VarDeclr (..)
    , VarValDeclr (..)
    , Statement (..)
    , SetStatement (..)
    , CallStatement (..)
    , CaseStatement (..)
    , Case (..)
    , ReturnStatement (..)
    , InLoopStatement (..)
    , LoopStatement (..)
    , Expr (..)
    , IntLiteral (..)
    , BinOp (..)
    , UnOp (..)
    , Type (..)
    , Name (..)
) where


import           Data.String (IsString (..))
import           Data.List.NonEmpty


data Program x = Program
    { declarations :: [Declr x]
    , functions :: [Function x]
    } deriving (Show, Eq)


data Declr x
    = Typedef Name Name
    | Globals [Global x]
    | Native Constness FuncDeclr
    deriving (Show, Eq)


data Constness
    = Constant
    | Variable
    deriving (Show, Eq)


data Global x
    = GlobalConst VarDeclr (Expr x)
    | GlobalVar (VarValDeclr x)
    deriving (Show, Eq)


data FuncDeclr = FuncDeclr
    { funcName :: Name
    , funcArgs :: [VarDeclr]
    , funcRetVal :: Maybe Type
    } deriving (Show, Eq)


data Function x = Function
    { funcConstness :: Constness
    , funcDeclr :: FuncDeclr
    , funcLocals :: [Local x]
    , funcStatements :: [Statement x]
    } deriving (Show, Eq)


newtype Local x = Local
    { localDeclr :: VarValDeclr x
    } deriving (Show, Eq)


data VarDeclr = VarDeclr
    { varType :: Type
    , varName :: Name
    } deriving (Show, Eq)


data VarValDeclr x
    = SimpleVar VarDeclr (Maybe (Expr x))
    | ArrayVar VarDeclr
    deriving (Show, Eq)


data Statement x
    = SetSttmnt (SetStatement x)
    | CallSttmnt (CallStatement x)
    | IfElifSttmnt (CaseStatement x)
    | LoopSttmnt (LoopStatement x)
    | ReturnSttmnt (ReturnStatement x)
    deriving (Show, Eq)


data SetStatement x = SetStatement
    { setVarName :: Name
    , setIndex :: Maybe (Expr x)
    , setValue :: Expr x
    } deriving (Show, Eq)


data CallStatement x = CallStatement
    { callFuncName :: Name
    , callArgs :: [Expr x]
    } deriving (Show, Eq)


data CaseStatement x = CaseStatement
    { caseConditionals :: NonEmpty (Case x)
    , caseDefault :: Maybe [Statement x]
    } deriving (Show, Eq)


data Case x = Case
    { caseCondition :: Expr x
    , caseBody :: [Statement x]
    } deriving (Show, Eq)


newtype ReturnStatement x = ReturnStatement
    { returnValue :: Maybe (Expr x)
    } deriving (Show, Eq)


data InLoopStatement x
    = UsualStatement (Statement x)
    | ExitWhen (Expr x)
    deriving (Show, Eq)


newtype LoopStatement x = LoopStatement
    { loopBody :: [InLoopStatement x]
    } deriving (Show, Eq)


data Expr x
    = NameRef x Name
    | BinExpr x BinOp (Expr x) (Expr x)
    | UnExpr x UnOp (Expr x)
    | FuncCall x Name [Expr x]
    | ArrayRef x Name (Expr x)
    | FuncRef Name
    | IntLit IntLiteral
    | RealLit Float
    | BoolLit Bool
    | StringLit String
    | Null
    deriving (Show, Eq, Functor)


data IntLiteral
    = DecimalLit Int
    | OctalLit Int
    | HexLit Int
    | FourCC Char Char Char Char
    deriving (Show, Eq)


data BinOp
    = JAdd
    | JSub
    | JMul
    | JDiv
    | JEqual
    | JNotEqual
    | JLess
    | JGreater
    | JLessEq
    | JGreaterEq
    | JAnd
    | JOr
    deriving (Show, Eq)


data UnOp
    = JUNeg
    | JUAdd
    | JUNot
    deriving (Show, Eq)


data Type
    = TypeName Name
    | JCode
    | JHandle
    | JInteger
    | JReal
    | JBoolean
    | JString
    deriving (Show, Eq)


newtype Name = Name
    { getName :: String
    } deriving (Show, Eq)


instance IsString (Expr x) where
    fromString = StringLit


instance IsString Name where
    fromString = Name


instance IsString Type where
    fromString = TypeName . Name

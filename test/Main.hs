module Main (
    main
) where

import           Test.Tasty
import           Test.Tasty.Golden

import           JassTests.Golden


main = do
    golden <- goldenTests
    defaultMain $ testGroup "Tests"
        [ golden
        ]

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Data.Jass.Codegen (
      renderJass
    , renderProgram
    , Jass (..)
) where

import           Data.Maybe
import           Data.Jass.Internal
import           Data.List.NonEmpty (toList)
import           Numeric
import           Text.PrettyPrint


indentSize :: Int
indentSize = 4


indent :: Doc -> Doc
indent = nest indentSize


vsep :: [Doc] -> Doc
vsep = foldr ($+$) empty


jassStyle :: Style
jassStyle = style
    { mode = PageMode
    , lineLength = maxBound
    , ribbonsPerLine = 0
    }


renderJass :: Doc -> String
renderJass = renderStyle jassStyle


renderProgram :: Program x -> String
renderProgram = renderJass . generate


class Jass a where
    generate :: a -> Doc

generateList :: (Jass a) => [a] -> Doc
generateList = hsep . punctuate "," . map generate

generateLines :: (Jass a) => [a] -> Doc
generateLines = vsep . map generate


funcCall :: Name -> [Expr x] -> Doc
funcCall func args =
    hcat
        [ generate func
        , parens $ generateList args
        ]


instance Jass (Program x) where
    generate Program{..} =
        vsep
            [ "//////////////////// declarations"
            , ""
            , generateLines declarations
            , ""
            , "//////////////////// functions"
            , ""
            , generateLines functions
            ]


instance Jass (Declr x) where
    generate (Typedef new old) =
        hsep
            [ "type"
            , generate new
            , "extends"
            , generate old
            ]
    generate (Globals xs) =
        vsep
            [ "globals"
            , indent $ generateLines xs
            , "endglobals"
            ]
    generate (Native constness func) =
        hsep
            [ generate constness
            , "native"
            , generate func
            ]


instance Jass (Function x) where
    generate Function{..} =
        vsep
            [ hsep
                [ generate funcConstness
                , "function"
                , generate funcDeclr
                ]
            , indent $ generateLines funcLocals
            , text ""
            , indent $ generateLines funcStatements
            , "endfunction"
            ]


instance Jass (Global x) where
    generate (GlobalConst declr value) =
        hsep
            [ "constant"
            , generate declr
            , "="
            , generate value
            ]
    generate (GlobalVar declr) =
        generate declr


instance Jass Constness where
    generate Constant = "constant"
    generate Variable = empty


instance Jass FuncDeclr where
    generate FuncDeclr{..} =
        hsep
            [ generate funcName
            , "takes"
            , case funcArgs of
                [] -> "nothing"
                xs -> generateList xs
            , "returns"
            , fromMaybe "nothing" $ generate <$> funcRetVal
            ]


instance Jass (Local x) where
    generate Local{..} =
        hsep
            [ "local"
            , generate localDeclr
            ]


instance Jass (Statement x) where
    generate (SetSttmnt st) = generate st
    generate (CallSttmnt st) = generate st
    generate (IfElifSttmnt st) = generate st
    generate (LoopSttmnt st) = generate st
    generate (ReturnSttmnt st) = generate st


instance Jass VarDeclr where
    generate VarDeclr{..} =
        hsep
            [ generate varType
            , generate varName
            ]

instance Jass Name where
    generate = text . getName


instance Jass (Expr x) where
    generate (NameRef _ n) = generate n
    generate (BinExpr _ op l r) =
        parens $ hsep
            [ generate l
            , generate op
            , generate r
            ]
    generate (UnExpr _ op val) =
        parens $ hsep
            [ generate op
            , generate val
            ]
    generate (FuncCall _ func args) = funcCall func args
    generate (ArrayRef _ var idx) =
        hcat
            [ generate var
            , brackets $ generate idx
            ]
    generate (FuncRef func) =
        hsep
            [ "code"
            , generate func
            ]
    generate (IntLit i) = generate i
    generate (RealLit f) =
        text $ show f
    generate (BoolLit b) =
        if b then "true" else "false"
    generate (StringLit s) = text s
    generate Null = "null"


instance Jass (VarValDeclr x) where
    generate (SimpleVar declr mval) =
        hsep
            [ generate declr
            , fromMaybe empty $ (("="<+>) . generate) <$> mval
            ]
    generate (ArrayVar VarDeclr{..}) =
        hsep
            [ generate varType
            , "array"
            , generate varName
            ]


instance Jass Type where
    generate (TypeName n) = generate n
    generate JCode = "code"
    generate JHandle = "handle"
    generate JInteger = "integer"
    generate JReal = "real"
    generate JBoolean = "boolean"
    generate JString = "string"


instance Jass (SetStatement x) where
    generate SetStatement{..} =
        hsep
            [ "set"
            , hcat
                [ generate setVarName
                , fromMaybe empty $ brackets . generate <$> setIndex
                ]
            , "="
            , generate setValue
            ]


instance Jass (CallStatement x) where
    generate CallStatement{..} =
        hsep
            [ "call"
            , funcCall callFuncName callArgs
            ]


instance Jass (CaseStatement x) where
    generate CaseStatement{..} =
        let
            genCase :: Case x -> (Doc, Doc)
            genCase Case{..} =
                let
                    cond = "if" <+> parens (generate caseCondition)
                    body = indent $ generateLines caseBody
                in (cond, body)
            prependElse :: (Doc, Doc) -> (Doc, Doc)
            prependElse (cond, body) = ("else" <> cond, body)

            defCase = case caseDefault of
                Nothing -> empty
                Just xs -> "else" $+$ indent (generateLines xs)

            cases = map genCase $ toList caseConditionals
            ifc : elifs = cases
            elifcs = map (uncurry ($+$) . prependElse) elifs
        in vsep
            [ uncurry ($+$) ifc
            , vsep elifcs
            , defCase
            , "endif"
            ]

instance Jass (LoopStatement x) where
    generate LoopStatement{..} =
        vsep
            [ "loop"
            , indent $ generateLines loopBody
            , "endloop"
            ]


instance Jass (ReturnStatement x) where
    generate ReturnStatement{..} =
        hsep
            [ "return"
            , fromMaybe empty $ generate <$> returnValue
            ]


instance Jass (InLoopStatement x) where
    generate (UsualStatement s) = generate s
    generate (ExitWhen cond) =
        hsep
            [ "exitwhen"
            , generate cond
            ]


instance Jass BinOp where
    generate JAdd = "+"
    generate JSub = "-"
    generate JMul = "*"
    generate JDiv = "/"
    generate JEqual = "=="
    generate JNotEqual = "!="
    generate JLess = "<"
    generate JGreater = ">"
    generate JLessEq = "<="
    generate JGreaterEq = ">="
    generate JAnd = "and"
    generate JOr = "or"


instance Jass UnOp where
    generate JUNeg = "-"
    generate JUAdd = "+"
    generate JUNot = "not"


instance Jass IntLiteral where
    generate (DecimalLit n)   = text $ show n
    generate (OctalLit n)     = text $ showOct n ""
    generate (HexLit n)       = text $ showHex n ""
    generate (FourCC a b c d) = text ['\'', a, b, c, d, '\'']

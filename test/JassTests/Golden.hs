{-# LANGUAGE OverloadedStrings #-}
module JassTests.Golden (
    goldenTests
) where


import           Control.Monad
import           Data.ByteString.Lazy.Char8 (pack)
import           Data.Jass.Combinators
import           Data.Jass.Codegen
import           Data.Jass.Types
import           Test.Tasty
import           Test.Tasty.Golden
import           System.FilePath (takeBaseName)


goldenTests :: IO TestTree
goldenTests = do
    let tests = flip map cases $ \(name, program) ->
            goldenVsString
                (takeBaseName name)
                name
                (return . pack $ renderProgram program)
    return $ testGroup "JASS golden tests" tests


cases :: [(String, Program ())]
cases =
    [ ("test/JassTests/small.j", small)
    ]


small :: Program ()
small = program decls funcs
    where
        decls = do
            "nh" `extends` "handle"
            globals $ do
                gconst JInteger "max_skel" $ int 123
                gvar JReal "skelspawn" $ int 0
            cnative (Just JReal) "GetWidgetLife" $
                arg JHandle "unit"
        funcs =
            cfunction Nothing "lolk" args locals body
                where
                    args =
                        arg JInteger "val"
                    locals =
                        lvar JInteger "i" $ int 0
                    i = "i"
                    skelspawn = "skelspawn"
                    body = do
                        loop $ do
                            exitwhen $ "i" >. int 10
                            inl $ do
                                set "i" ("i" +. int 1)
                                set ("skelspawn" `at` i) $ int 0
                        if' $ (case' ("i" ==. int 10) $
                                set "i" (int 11))
                            `elif` (case' ("i" ==. int 11) $
                                set "i" (int 12))
                            `else'`
                                set "i" (int 999)



